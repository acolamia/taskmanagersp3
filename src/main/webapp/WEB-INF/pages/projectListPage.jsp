<%--
  Created by IntelliJ IDEA.
  User: agris
  Date: 03.12.2019
  Time: 16:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>TASK MANAGER</title>
</head>
<body>
<h1 align="right" class="display-4">PROJECTS</h1>
<table class="table table-bordered table-sm">
    <thead>
    <tr>
        <th scope="col"><p align="center">№</p></th>
        <th scope="col"><p align="center">ID</p></th>
        <th scope="col"><p align="center">NAME</p></th>
        <th scope="col"><p align="center">DESCRIPTION</p></th>
        <th scope="col"><p align="center">DATE BEGIN</p></th>
        <th scope="col"><p align="center">DATE END</p></th>
        <th scope="col"><p align="center">STATUS</p></th>
        <th scope="col" colspan="3"><p align="center">ACTION</p></th>
    </tr>
    </thead>
    <c:set var="count" value="0"/>
    <c:forEach var="project" items="${projectList}">
        <c:set var="count" value="${count + 1}"/>
        <tbody>
        <tr>
            <td align="center" scope="row">${count}</td>
            <td align="center">${project.id}</td>
            <td align="center">${project.name}</td>
            <td align="center">${project.description}</td>
            <td align="center">${project.dateBegin}</td>
            <td align="center">${project.dateEnd}</td>
            <td align="center">${project.statusType}</td>
            <td>
                <a href="/taskList/${project.id}">
                    <div align="center"><font color="blue">VIEW</font></div>
                </a>
            </td>
            <td>
                <a href="/editProject/${project.id}">
                    <div align="center"><font color="#b8860b">EDIT</font></div>
                </a>
            </td>
            <td>
                <a href="/deleteProject/${project.id}">
                    <div align="center"><font color="#dc143c">DELETE</font></div>
                </a>
            </td>
            </td>
        </tr>
        </tbody>
    </c:forEach>
</table>
<c:url value="/addProject" var="var"/>
<a href="${var}"><button type="button" class="btn btn-success">ADD NEW PROJECT</button></a>
<c:url value="/" var="main"/>
<a href="${main}"><button type="button" class="btn btn-secondary">GO TO MAIN</button></a>
</body>
</html>
