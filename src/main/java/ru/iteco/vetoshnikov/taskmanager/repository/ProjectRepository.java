package ru.iteco.vetoshnikov.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.iteco.vetoshnikov.taskmanager.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project,String> {
//    @NotNull
//    private final static Map<String, Project> projectMap = new HashMap<>();
//
//    @Nullable
//    @Override
//    public List<Project> allProjects() {
//        return new ArrayList<>(projectMap.values());
//    }
//
//    @Override
//    public void add(@NotNull final Project project) {
//        projectMap.put(project.getId(), project);
//    }
//
//    @Override
//    public void delete(@NotNull final Project project) {
//        projectMap.remove(project.getId());
//    }
//
//    @Override
//    public void edit(@NotNull final Project project) {
//        projectMap.put(project.getId(), project);
//    }
//
//    @Nullable
//    @Override
//    public Project getById(@NotNull final String id) {
//        return projectMap.get(id);
//    }
}
