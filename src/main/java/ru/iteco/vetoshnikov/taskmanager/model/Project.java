package ru.iteco.vetoshnikov.taskmanager.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.Nullable;
import org.hibernate.annotations.Cache;
import org.springframework.format.annotation.DateTimeFormat;
import ru.iteco.vetoshnikov.taskmanager.constant.StatusType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "projects")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Project extends AbstractModel {
    @Column(name = "name")
    @Nullable
    private String name;

    @Column(name = "description")
    @Nullable
    private String description;

    @Column(name = "dateBegin")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateBegin;

    @Column(name = "dateEnd")
    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateEnd;

    @Column(name = "statusType")
    @Enumerated(EnumType.STRING)
    private StatusType statusType = StatusType.PLANNED;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    private List<Task> taskList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public StatusType getStatusType() {
        return statusType;
    }

    public void setStatusType(StatusType statusType) {
        this.statusType = statusType;
    }
}
